" :::: Hans Heums vimrc ::::
" inspirasjonskilde: https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/

" VUNDLE
set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)

" SimpylFold: A plugin to make vim fold python code correctly
Plugin 'tmhedberg/SimpylFold'

" enable folding with the spacebar
nnoremap <space> zc

" if you want to see the docstrings for folded code
let g:SimpylFold_docstring_preview=1

" indentpython.vim: A plugin to fix auto-indentation for Python
" outdated/abandoned?
"Plugin 'vim-scripts/indentpython.vim'
" use this instead:
Plugin 'Vimjas/vim-python-pep8-indent'

" YouCompleteMe: Python autocompletion plugin (får den ikke til å funke...trenger mest sannsynlig macvim (mvim i terminalen), men det er alt for irriterende å bruke siden det åpner alle dokumenter i nye vindu)
"Plugin 'Valloric/YouCompleteMe'
" 	Makes the window go away when you're done with it
"let g:ycm_autoclose_preview_window_after_completion=1
" 	Makes space-g goto definition of whatever I'm currently on (needs the
" 	currently-not-working virtualenv support to work)
"map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

" syntastic: Makes Vim check your syntax on each save
Plugin 'vim-syntastic/syntastic'

" vim-flake8: Makes Vim check your PEP8 on each save
Plugin 'nvie/vim-flake8'

" Dark colour scheme
Plugin 'jnurmine/Zenburn'
set t_Co=256 " Enable 256-color mode for Vim, needed for Zenburn to render correctly
colors zenburn 

" Light colour scheme
"syntax enable
"set background=light
"colorscheme solarized

" VIL HA DENNE! Men installasjonen funker ikke nå. Reinstaller?
" Fix
" fra ~/.vim/bundle/powerline, kjør python setup.py egg_info
" Powerline! (Funker kun når flere vinduer er åpne? Sjekk http://powerline.readthedocs.io/en/latest/)
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
" Løsning på ovennevnte problem; se også :help laststatus
set laststatus=2

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" /VUNDLE

" Make your code look pretty! Enables syntax highlighting
let python_highlight_all=1
syntax on 

" Show cursor position (as numbers and percentage, bottom right)
set ruler 

" Encoding
set encoding=utf-8

" Turn on line numbering
set number

" Scroll up/down when within n lines of top/bottom
set scrolloff=12

" Have backspace behave normally
" indent = allow backspacing over autoindent
" eol = allow backspacing over line breaks
" start = allow backspacing over the start of insert
set backspace=indent,eol,start

" Make tabs 2 instead of 8 spaces wide
set tabstop=2
set softtabstop=2 
set expandtab
set shiftwidth=2

" set automatic line break at first whitespace after given number of characters
set tw=79

" set parapgraph indentation behaviour (default: tcq; see :help fo-table)
set fo=tcq2

" Proper Python PEP8 indentation
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

" Define BadWhitespace before using in a match
" highlight BadWhitespace ctermbg=red guibg=darkred

" Flag redundant whitespace as 'bad'
" au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
" ^Var egentlig bare irriterende. Kan kanskje være nyttig, men da må fargen
" endres.

" Useful keyboard maps for norwegian keyboards
noremap > ~
noremap & ^

" Enter for å sette inn ny linje under uten å gå inn i insert mode
" Ctrl-o for å sette inn linjen over
nmap <C-o> O<Esc>
nmap <CR> o<Esc>

" Hadde denne på før, men tror ikke jeg trenger den lenger:
" filetype indent plugin on

" Tells Vim where window splits should occur
set splitbelow
set splitright

" Split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

" Python with virtualenv support (doesn't work)
"py << EOF
"import os
"import sys
"if 'VIRTUAL_ENV' in os.environ:
" project_base_dir = os.environ['VIRTUAL_ENV']
" activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
" execfile(activate_this, dict(__file__=activate_this))
"EOF

" integrate Vim clipboard with system clipboard
set clipboard=unnamed

" custom digraphs
" (inspired by the list at the bottom of this page https://silq.ethz.ch/documentation)
" Blackboard Bold
dig nn 8469
dig rr 8477
dig bb 120121
dig 11 120793
" xor symbol
dig op 8853
" tilde
dig ti 126

" get vim to recognize .slq as D language files
autocmd BufEnter *.slq :setlocal filetype=d

